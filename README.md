1. Test one change
2. Test second change
3. Test change in Source tree



# INFO230 - Assigment1

how to put index.html on web

While it is not ideal for creating an entire website, we do offer users the ability to create a custom index.html page in cPanel with just a few steps.
This is a helpful solution for creating a Website coming soon page with your email address, phone number, hours of operation, etc.

How to set up a custom index.html file
Log into your Hosting cPanel.
Click File Manager in the Files section, then select the Web Root radio button.
Locate the current index.html page and delete or rename it. If you have just set up your hosting account, the index.html file will be the branded Name.com Coming soon page. 
You can highlight the file and click delete at the top of the screen.
Click New File in the upper left corner and name the file index.html.
Highlight the file by clicking once and then click the HTML Editor button at the top of the screen. Then click the Edit button.
You can now create the file. It works very similar to word processing software and you can enter text, change fonts, sizes, colors, etc. You can also upload images and insert hyperlinks.

